//==============================================================================
//
//       Filename:  hash.cpp
//
//    Description:  test for the hashing of metadata files
//
//        Version:  1.0
//        Created:  06/27/2015 02:58:42 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <iostream>
#include <iomanip>
#include <fstream>
#include <openssl/sha.h>

#include "bencode.hpp"
#include "bencode_parser.hpp"

std::string to_hex(const std::string& s){
  static char table[] = "0123456789abcdef";
  std::string result;
  result.reserve(s.size()*2);
  for(auto c : s){
    result.push_back(table[(0xf0 & c)>>4]);
    result.push_back(table[ 0x0f & c ]);
  }
  return result;
}

int main(){

  using It = boost::spirit::istream_iterator;
  std::ifstream file{"test.torrent"};
  file.unsetf(std::ios::skipws);
  It begin{file}, end{};
  torr::bencode::Bencode metadata;
  torr::bencode::BencodeParser<It>{}.parse(begin, end, metadata);
  auto metainfo = metadata["info"].string();

  std::string hash;
  hash.resize(20);
  SHA1(reinterpret_cast<const unsigned char*>(metainfo.data()), metainfo.size(),
       reinterpret_cast<unsigned char*>(&hash.front()));

  std::cout << to_hex(hash);
  return 0;
}
