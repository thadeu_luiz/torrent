//==============================================================================
//
//       Filename:  server_double.hpp
//
//    Description:  include for server double definitions
//
//        Version:  1.0
//        Created:  06/04/2015 11:11:52 AM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#ifndef server_double_HPP
#define server_double_HPP

#include <memory>
#include <map>
#include <fstream>
#include <thread>

#include <boost/asio.hpp>

namespace torr {
namespace testing {

  using std::string;
  using std::shared_ptr;
  using std::streambuf;
  using std::istream;
  using Service = boost::asio::io_service;
  using Socket = boost::asio::ip::tcp::socket;
  using Acceptor = boost::asio::ip::tcp::acceptor;

  class TcpConnection : public std::enable_shared_from_this<TcpConnection> {
   public:
    using ptr = shared_ptr<TcpConnection>;

    template <class... Args>
    static ptr create(Args&&... args) {
      return ptr{new TcpConnection{std::forward<Args>(args)...}};
    }

    void write(string);
    Socket& socket() { return socket_; }

    TcpConnection() = delete;
    TcpConnection(const TcpConnection&) = delete;
    TcpConnection(TcpConnection&&) = delete;
    ~TcpConnection();

   private:
    TcpConnection(Service& service);
    Socket socket_;
    string message_;
  };

  class ServerDouble {
   public:
    ServerDouble(const string& ="test_double_responses.txt", unsigned short port=3001);
    ~ServerDouble();

   private:
    void startAccept_();
    void onAccept_(TcpConnection::ptr);
    void loadResponses_(istream&, const string&);
    void onRead_(streambuf&, TcpConnection::ptr);
    string getResponse_(const string&) const;


    Service service_;
    std::map<string, string> responses_;
    std::thread worker_;
    Acceptor acceptor_;
  };

}  // -----  end of namespace testing  -----
}  // -----  end of namespace torr  -----

#endif  // ----- #ifndef server_double_HPP  -----
