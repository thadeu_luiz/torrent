set(test_libs gmock gtest pthread)

file(GLOB test_sources *Test.cpp)

#disable annoying gtest warnings)
add_definitions(-Wno-weak-vtables -Wno-global-constructors)
include_directories(.)

find_package(Boost COMPONENTS system REQUIRED)

#BencodeUnitTest
configure_file(test.torrent test.torrent COPYONLY)
add_executable(BencodeTests test.cpp BencodeTest.cpp BencodeParserTest.cpp)
target_link_libraries(BencodeTests ${test_libs} bencode)
add_test(BencodeTest BencodeTests)

configure_file(request_fixtures.txt test_double_responses.txt COPYONLY)
add_library(server_double server_double.cpp)
target_link_libraries(server_double ${Boost_SYSTEM_LIBRARY})
add_executable(ServerDoubleTests test.cpp ServerDoubleTest.cpp)
target_link_libraries(ServerDoubleTests ${test_libs} server_double)
add_test(ServerDoubleTest ServerDoubleTests)
