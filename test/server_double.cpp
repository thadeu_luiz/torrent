//==============================================================================
//
//       Filename:  server_double.cpp
//
//    Description:  server double for tests, responds with hardcoded answers
//
//        Version:  1.0
//        Created:  06/04/2015 11:06:25 AM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <memory>
#include <stdexcept>
#include <boost/asio.hpp>
#include <iostream>

#include "server_double.hpp"

using namespace std;
namespace asio = boost::asio;
namespace sys = boost::system;

using asio::ip::tcp;

namespace torr {
namespace testing {

  //----------------------------------------------------------------------------
  //  TcpConnection
  //----------------------------------------------------------------------------
  TcpConnection::TcpConnection(Service& service) : socket_{service} {
  }

  TcpConnection::~TcpConnection(){
  }

  void TcpConnection::write(string message) {
    message_ = move(message);
    async_write(socket_, asio::buffer(message_),
                [ptr = shared_from_this()](const auto& error, auto) {
                  if (error) throw boost::system::system_error{error};
                });
  }

  //----------------------------------------------------------------------------
  //  ServerDouble
  //----------------------------------------------------------------------------
  ServerDouble::ServerDouble(const string& file_name, unsigned short port)
      : acceptor_{service_, tcp::endpoint{tcp::v4(), port}} {
    std::ifstream file{file_name, ios::in};
    loadResponses_(file, file_name);
    startAccept_();
    worker_ = thread{[this]() { this->service_.run(); }};
  }

  ServerDouble::~ServerDouble() {
    service_.stop();
    worker_.join();
  }

  void ServerDouble::startAccept_() {
    auto connection = TcpConnection::create(service_);
    acceptor_.async_accept(
        connection->socket(),
        [ connection = connection, this ](const auto& error) {
          if (error) throw sys::system_error{error};
          this->onAccept_(move(connection));
          this->startAccept_();
        });
  }
  
  // prepare read
  void ServerDouble::onAccept_(TcpConnection::ptr connection) {
    auto buff_ptr = make_shared<asio::streambuf>();
    async_read_until(connection->socket(), *buff_ptr, "\r\n\r\n",
        [ buff_ptr = buff_ptr, connection, this ](const auto& error, size_t) {
          if (error)
            throw sys::system_error{error};
          this->onRead_(*buff_ptr, std::move(connection));
        });
  }

  void ServerDouble::onRead_(streambuf& buffer, TcpConnection::ptr connection) {
    istreambuf_iterator<char> begin(&buffer), end;
    string request(begin, end);
    auto eol = request.find("\r\n");
    auto response = getResponse_(request.substr(0, eol));
    connection->write(response);
  }

  string ServerDouble::getResponse_(const string& request) const {
    auto it = responses_.find(request);
    if (it == responses_.end()) return "Error\n";
    return it->second;
  }

  void ServerDouble::loadResponses_(istream& file, const string& file_name) {
    if (!file) throw runtime_error{"Server::Unable to read file: " + file_name};

    for (string key; getline(file, key);) {//read entire file
      size_t response_length;
      file >> response_length;
      file.ignore(256, '\n');

      if (!response_length)
        throw runtime_error{"Server::bad file format: " + file_name};

      string response(response_length, '\0');
      file.read(&response[0], static_cast<long>(response_length));
      response.resize(static_cast<size_t>(file.gcount()));

      if (response.size() != response_length)
        throw runtime_error{"Server::unexpected file end: " + file_name};
      responses_[key] = response;
    }
  }

}  // -----  end of namespace testing  -----
}  // -----  end of namespace torr  -----
