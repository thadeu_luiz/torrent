//==============================================================================
//
//       Filename:  ServerDoubleTest.cpp
//
//    Description:  testing for the double
//
//        Version:  1.0
//        Created:  06/04/2015 06:13:54 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
//==============================================================================

#include <iostream>
#include "gmock/gmock.h"

#include "server_double.hpp"

using namespace torr::testing;

TEST(AServerDouble, ShouldBeCreatedWithoutThrowing){
  ServerDouble server;
}
