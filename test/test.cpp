// =============================================================================
//
//       Filename:  test.cpp
//
//    Description:  Main file for all the tests
//
//        Version:  1.0
//        Created:  05/14/2015 08:42:20 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================

#include "gmock/gmock.h"

using namespace testing;

int main(int argc, char* argv[]){
  InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}

