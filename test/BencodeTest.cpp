// =============================================================================
//
//       Filename:  BencodeTest.cpp
//
//    Description:  Bencode Struct Test
//
//        Version:  1.0
//        Created:  05/14/2015 10:54:33 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================

#include <iostream>
#include "gmock/gmock.h"

#include "bencode.hpp"

using namespace torr::bencode;
using namespace torr::bencode::detail;
using namespace ::testing;
using boost::get;

TEST(BencodeStructure, DefaultConstructsWithAnInt) {
  Bencode bencode;

  ASSERT_THAT(bencode.whichType(), Eq(Type::BencodeInt));
}

TEST(BencodeStructure, CanBeCreatedWithInt) {
  BencodeInt b_int{10};
  Bencode bencode{b_int};

  ASSERT_THAT(bencode.whichType(), Eq(Type::BencodeInt));
  EXPECT_THAT(bencode, Eq(Bencode{10}));
}

TEST(BencodeStructure, CanBeCreatedWithString) {
  BencodeString a_string{"A random string"};
  Bencode bencode{a_string};

  ASSERT_THAT(bencode.whichType(), Eq(Type::BencodeString));
  EXPECT_THAT(bencode, Eq(Bencode{"A random string"}));
}

TEST(BencodeStructure, CanBeCreatedWithAList) {
  Bencode string_node{"String"}, int_node{90};
  BencodeList a_list_of_strings(3, string_node), a_list_of_ints{10, int_node};

  Bencode string_list_bencode{a_list_of_strings},
      int_list_bencode{a_list_of_ints}, empty_list_bencode{BencodeList{}};

  ASSERT_THAT(string_list_bencode.whichType(), Eq(Type::BencodeList));

  EXPECT_THAT(string_list_bencode, Eq(string_list_bencode));
  EXPECT_THAT(string_list_bencode, Ne(int_list_bencode));
  EXPECT_THAT(string_list_bencode, Ne(empty_list_bencode));
}

TEST(BencodeStructure, CanBeCreatedWithAMap) {
  BencodeString key{"A key"};
  Bencode string_node{"String"}, int_node{90};
  BencodeDictionary key_and_string{{key, string_node}},
      key_and_int{{key, int_node}};

  ASSERT_THAT(Bencode{key_and_string}.whichType(), Eq(Type::BencodeDictionary));
  EXPECT_THAT(Bencode{key_and_string}, Eq(Bencode{key_and_string}));
  EXPECT_THAT(Bencode{key_and_string}, Ne(Bencode{key_and_int}));
}

TEST(BencodeStructure, CanBeCopyConstructed) {
  Bencode bencode{ "a value" };

  auto bencode_copy = bencode;
  Bencode bencode_copy2 { bencode };

  ASSERT_THAT(bencode, Eq(bencode_copy));
  ASSERT_THAT(bencode, Eq(bencode_copy2));

}

class NestedBencode : public Test {
 public:
  Bencode inner_bencode{1283};
  BencodeString key{"A key"};
  BencodeList bencode_list{inner_bencode};
  BencodeDictionary bencode_dictionary{{key, inner_bencode}};
};

TEST_F(NestedBencode, ThrowsWhenAccessingAnIncorrectContainer) {
  Bencode list_bencode{bencode_list};
  Bencode dic_bencode{bencode_dictionary};

  ASSERT_THROW(list_bencode[key], boost::bad_get);
  ASSERT_THROW(dic_bencode[0U], boost::bad_get);

  EXPECT_THAT(dic_bencode[key], Eq(inner_bencode));
  EXPECT_THAT(list_bencode[0U], Eq(inner_bencode));
}

TEST(BencodePrint, PrintsWithTheRightFormatting){
  Bencode bencode {
    BencodeList{
      Bencode{}, Bencode{"some text"}, Bencode{99},
      Bencode{BencodeDictionary{ {"a key", Bencode{"A value"}}, {"Another Key", Bencode{9}},
        {"key",  Bencode{BencodeDictionary{ {"Nested Key" , Bencode{"Nested Value"}}  }} }
      }}
    }
  };

//using the expected result here
std::string expected{
R"END([
  0
  some text
  99
  {
    Another Key = 9
    a key = A value
    key = {
            Nested Key = Nested Value
          }
  }
]
)END"};
  std::stringstream ss;
  ss << bencode;
  EXPECT_THAT(ss.str(), Eq(expected));
}

TEST(BencodeStringVisitor, ConvertsIntToStringAsExpected){
  Bencode int_bencode{10};
  EXPECT_THAT(int_bencode.string(), Eq("i10e"));
}

TEST(BencodeStringVisitor, ConvertsStringAsExpected){
  Bencode string_bencode{"test"};
  EXPECT_THAT(string_bencode.string(), Eq("4:test"));
}

TEST(BencodeStringVisitor, ConvertsListAsExpected){
  Bencode list_bencode{BencodeList{Bencode{6}, Bencode{"lol"}}};
  EXPECT_THAT(list_bencode.string(), Eq("li6e3:lole"));
}

TEST(BencodeStringVisitor, ConvertsDictionaryAsExpected){
  Bencode dic_bencode{
    BencodeDictionary{
      {"test", Bencode{10}}
    }
  };
  EXPECT_THAT(dic_bencode.string(), Eq("d4:testi10ee"));
}
