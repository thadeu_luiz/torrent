// =============================================================================
//
//       Filename:  BencodeParserTest.cpp
//
//    Description:  Bencoder Parser Test
//
//        Version:  1.0
//        Created:  05/14/2015 10:53:45 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================

#include <fstream>
#include <iterator>

#include "gmock/gmock.h"
#include "bencode.hpp"
#include "bencode_parser.hpp"

using namespace torr::bencode;
using namespace ::testing;

class ABencodeParser : public Test {
 public:
  BencodeParser<BencodeString::const_iterator> parser;
  Bencode result;

  bool parse(const BencodeString& str) {
    return parser.parse(str.begin(), str.end(), result);
  }
};

TEST_F(ABencodeParser, ParsesAnWellFormedInteger) {
  BencodeString int_string{"i10e"};
  EXPECT_THAT(parse(int_string), Eq(true));
}

TEST_F(ABencodeParser, RetrievesTheValueOfAnWellFormedInteger) {
  BencodeString int_string{"i10e"};
  parse(int_string);
  EXPECT_THAT(result, Eq(Bencode{10}));
}

TEST_F(ABencodeParser, RejectsIllFormedInteger) {
  BencodeString bad_int_string{"i120938 e"};
  EXPECT_THAT(parse(bad_int_string), Eq(false));
}

TEST_F(ABencodeParser, IgnoresSpaceAfterInteger) {
  BencodeString bad_int_string{"i120938e "};
  EXPECT_THAT(parse(bad_int_string), Eq(true));
}

TEST_F(ABencodeParser, ParsesAWellFormedString) {
  BencodeString b_string{"5:f uba"};
  parse(b_string);
  EXPECT_THAT(result, Eq(Bencode{"f uba"}));
}

TEST_F(ABencodeParser, ParsesAStringWithNullBytes) {
  BencodeString target_string;
  target_string.push_back('\0');
  target_string.append("bar");
  BencodeString text = "4:" + target_string;

  parse(text);

  EXPECT_THAT(result, Eq(Bencode{target_string}));
}

TEST_F(ABencodeParser, ParsesAWellFormedList) {
  BencodeList list{Bencode{0}, Bencode{1}};
  BencodeString list_string{" li0ei1ee "};

  parse(list_string);

  EXPECT_THAT(result, Eq(Bencode{list}));
}

TEST_F(ABencodeParser, ParsesAWellFormedDictionary) {
  BencodeDictionary dictionary;
  dictionary["a key"] = Bencode{"A"};
  dictionary["b key"] = Bencode{"B"};
  BencodeString dictionary_string{"d 5:a key  1:A   5:b key  1:B e"};

  parse(dictionary_string);

  EXPECT_THAT(result, Eq(Bencode{dictionary}));
}

TEST_F(ABencodeParser, ParsesAValidTorrentFile) {
  std::ifstream file{"test.torrent"};
  BencodeString file_string{std::istreambuf_iterator<char>{file},
                            std::istreambuf_iterator<char>{}};
  file.close();

  auto response = parse(file_string);

  EXPECT_THAT(response, Eq(true));
}

TEST(ADirectBencodeParser, ParsesAFileDirectly){
  using It = boost::spirit::istream_iterator;
  std::ifstream file{"test.torrent", std::ios::binary};
  file.unsetf( std::ios::skipws);
  It begin{file}, end{};
  BencodeParser<It> parser;
  Bencode result;
  auto response = parser.parse(begin, end, result);

  EXPECT_THAT(response, Eq(true));
}
