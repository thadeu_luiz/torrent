// =============================================================================
//
//       Filename:  bencode.hpp
//
//    Description:  Bencode Struct Definition
//
//        Version:  1.0
//        Created:  05/14/2015 10:55:14 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================

#ifndef bencode_HPP
#define bencode_HPP

#include <string>
#include <vector>
#include <map>
#include <type_traits>
#include <cstdint>
#include <memory>
#include <iosfwd>

#include <boost/variant/variant.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/get.hpp>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/operations.hpp>

namespace torr {
namespace bencode {
  class Bencode;

  bool operator==(const Bencode&, const Bencode&);
  bool operator!=(const Bencode&, const Bencode&);
  std::ostream& operator<<(std::ostream&, const Bencode&);

  using boost::recursive_wrapper;
  using boost::variant;

  using BencodeInt = std::int64_t;  // have to use 64bit to handle large files
  using BencodeString = std::string;
  using BencodeList = std::vector<recursive_wrapper<Bencode>>;
  using BencodeDictionary = std::map<BencodeString, recursive_wrapper<Bencode>>;
  using BencodePair = std::pair<BencodeString, recursive_wrapper<Bencode>>;

  namespace detail {
    enum class Type {
      BencodeInt,
      BencodeString,
      BencodeList,
      BencodeDictionary
    };
  }

  class Bencode : public variant<BencodeInt, BencodeString, BencodeList,
                                 BencodeDictionary> {
   private:
    template <class T>
    constexpr static bool isBencode_() {
      return std::is_same<Bencode, typename std::decay<T>::type>::value;
    }
    using BaseClass =
        variant<BencodeInt, BencodeString, BencodeList, BencodeDictionary>;

   public:
    // Ctors
    Bencode() = default;

    //wont interfere with copy construction
    template <class T, class = std::enable_if_t<!isBencode_<T>()>>
    explicit Bencode(T&& t)
        : BaseClass{std::forward<T>(t)} {}

    // Accesors to the containers, its ok, it may throw
    const Bencode& operator[](BencodeList::size_type pos) const;
    const Bencode& operator[](const BencodeString& key) const;
    BencodeString string() const;

    // Forwards the arg (key or n) to the const version and removes constness
    template <class T>
    Bencode& operator[](const T& t) {
      return const_cast<Bencode&>(const_cast<const Bencode&>(*this)[t]);
    }

    detail::Type whichType() const;
  };

  namespace detail {

    class BencodeStringVisitor: public boost::static_visitor<void>{
     public:
      BencodeStringVisitor(std::ostream&);

      void operator()(const BencodeInt&) const;
      void operator()(const BencodeString&) const;
      void operator()(const BencodeList&) const;
      void operator()(const BencodeDictionary&) const;

     private:
      std::ostream& os_;
    };

    class BencodeStrictEqualsVisitor : public boost::static_visitor<bool> {
     public:
      template <typename T, typename U>
      bool operator()(const T&, const U&) const {
        return false;  // cannot compare different types
      }

      bool operator()(const BencodeString&, const BencodeString&) const;
      bool operator()(const BencodeInt&, const BencodeInt&) const;
      bool operator()(const BencodeList&, const BencodeList&) const;
      bool operator()(const BencodeDictionary&, const BencodeDictionary&) const;

     private:
      template <class Container, class BinaryPredicate>
      bool testEqualityOn_(const Container& lhs, const Container& rhs,
                           const BinaryPredicate& pred) const {
        return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), pred);
      }
    };

    class IndentOutputFilter : public boost::iostreams::output_filter {
     public:
      // the number of indenting spaces and the initial newline state
      IndentOutputFilter(std::size_t, bool = false);
      template <class Sink>
      bool put(Sink& sink, char c) {
        if (c == '\n')
          new_line_ = true;
        else if (new_line_) {
          for (std::size_t n = 0; n < indent_; ++n)
            boost::iostreams::put(sink, ' ');
          new_line_ = false;
        }
        return boost::iostreams::put(sink, c);
      }

     private:
      std::size_t indent_;
      bool new_line_;
    };

    class BencodePrinter : public boost::static_visitor<> {
     public:
      BencodePrinter(std::ostream&);

      void operator()(const BencodeString&) const;
      void operator()(const BencodeInt&) const;
      void operator()(const BencodeList&) const;
      void operator()(const BencodeDictionary&) const;

     private:
      using FilteringOstream = boost::iostreams::filtering_ostream;
      using OstreamPtr = std::unique_ptr<FilteringOstream>;

      std::ostream& os_;
      const static size_t INDENT_;

      void printPair_(const BencodePair&) const;
      OstreamPtr makeFilteredOstream_(size_t, bool) const;
      std::size_t getHashIndent_(const BencodeString&) const;
    };
  }  // -----  end of namespace detail  -----

}  // -----  end of namespace bencode  -----
}  // -----  end of namespace torr  -----
#endif  // ----- #ifndef bencode_HPP  -----

