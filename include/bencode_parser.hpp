// =============================================================================
//
//       Filename:  bencode_parser.hpp
//
//    Description:  Parser for the Bencode struct using Boost.spirit
//
//        Version:  1.0
//        Created:  05/15/2015 11:05:55 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================

#ifndef bencode_parser_HPP
#define bencode_parser_HPP
#include <iostream>

#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/home/qi.hpp>
#include <boost/phoenix.hpp>

#include "bencode.hpp"

namespace torr {
namespace bencode {

  namespace qi = boost::spirit::qi;
  using qi::ascii::space_type;

  template <class Iterator>
  class BencodeParser : public qi::grammar<Iterator, Bencode(), space_type> {
   public:
    BencodeParser() : BencodeParser::base_type{bencode_} {
      using namespace qi;

      int_ %= lexeme['i' >> long_ >> 'e'];
      string_ %= lexeme[omit[ulong_[_a = _1]] >> ':' >> repeat(_a)[byte_]];
      list_ %= 'l' >> *bencode_ >> 'e';
      dictionary_ %= 'd' >> *(string_ >> bencode_) >> 'e';
      bencode_ %= (int_ | string_ | list_ | dictionary_);

    }

    bool parse(Iterator begin, Iterator end, Bencode& result) {
      using namespace qi;
      auto p = phrase_parse(begin, end, *this, ascii::space, result);
      return p && (begin == end);
    }

   private:
    qi::rule<Iterator, BencodeInt(), space_type> int_;
    qi::rule<Iterator, BencodeString(), qi::locals<unsigned long>, space_type>
        string_;
    qi::rule<Iterator, BencodeList(), space_type> list_;
    qi::rule<Iterator, BencodeDictionary(), space_type> dictionary_;
    qi::rule<Iterator, Bencode(), space_type> bencode_;
  };

}  // -----  end of namespace Bencode  -----
}  // -----  end of namespace Torr // -----
#endif  // ----- #ifndef bencode_parser_HPP  -----
