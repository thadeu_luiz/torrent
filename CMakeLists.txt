cmake_minimum_required (VERSION 2.8)
set(CMAKE_CXX_COMPILER "/usr/bin/clang++")
set(BOOST_ROOT ${CMAKE_SOURCE_DIR}/lib/boost_1_58_0/)

project(Torrent)


set(warnings "-Wfatal-errors -Wall -Weffc++ -Wextra -pedantic -Weverything\
              -Wno-c++98-compat")

set(CMAKE_CXX_FLAGS ${warnings})

add_definitions(-g)
add_definitions(-std=c++14 )

#set up googleTest
include_directories(SYSTEM "lib/gmock-1.7.0/include/")
include_directories(SYSTEM "lib/gmock-1.7.0/gtest/include/")

add_subdirectory("lib/gmock-1.7.0")
set_property(TARGET gmock gtest gmock_main gtest_main
             APPEND_STRING PROPERTY COMPILE_FLAGS " -w ")

#set up boost
include_directories(SYSTEM "lib/boost_1_58_0/")

#set up local libs
include_directories("include")
add_subdirectory("src")

#set up testing execs
enable_testing()
add_subdirectory("test")

#some testing codes...
add_subdirectory("scrap_codes")
