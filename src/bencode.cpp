// =============================================================================
//
//       Filename:  bencode.cpp
//
//    Description:  Bencode Implementation file
//
//        Version:  1.0
//        Created:  05/16/2015 02:09:32 PM
//       Revision:  none
//       Compiler:  g++
//
//         Author:  Thadeu Luiz Barbosa Dias (tlbd), thadeuluiz@poli.ufrj.br
//
// =============================================================================
#include <sstream>

#include "bencode.hpp"

namespace torr {
namespace bencode {

  // ===========================================================================
  //       Class:  Bencode
  // ===========================================================================
  const Bencode& Bencode::operator[](BencodeList::size_type pos) const {
    return boost::get<BencodeList>(*this).at(pos).get();
  }
  const Bencode& Bencode::operator[](const BencodeString& key) const {
    return boost::get<BencodeDictionary>(*this).at(key).get();
  }
  BencodeString Bencode::string() const {
    std::stringstream ss;
    boost::apply_visitor(detail::BencodeStringVisitor{ss}, *this);
    return ss.str();
  }
  detail::Type Bencode::whichType() const {
    return static_cast<detail::Type>(which());
  }

  namespace detail {
    // =========================================================================
    //        Class:  BencodeStringVisitor
    // =========================================================================
    BencodeStringVisitor::BencodeStringVisitor(std::ostream& os):os_{os}{}
    void BencodeStringVisitor::operator()(const BencodeInt& i) const {
      os_ << "i" << i << "e";
    }
    void BencodeStringVisitor::operator()(const BencodeString& s) const {
      os_ << s.size() << ":" << s;
    }
    void BencodeStringVisitor::operator()(const BencodeList& list) const {
      os_ << "l";
      for(auto const& node : list)
        boost::apply_visitor(*this, node.get());
      os_ << "e";
    }
    void BencodeStringVisitor::operator()(const BencodeDictionary& dic) const {
      os_ << "d";
      for(auto const& pair: dic){
        this->operator()(pair.first);
        boost::apply_visitor(*this, pair.second.get());
      }
      os_ << "e";
    }
    // =========================================================================
    //        Class:  BencodeStrictEqualsVisitor
    // =========================================================================
    bool BencodeStrictEqualsVisitor::operator()(
        const BencodeString& lhs, const BencodeString& rhs) const {
      return lhs == rhs;
    }
    bool BencodeStrictEqualsVisitor::operator()(const BencodeInt& lhs,
                                                const BencodeInt& rhs) const {
      return lhs == rhs;
    }
    bool BencodeStrictEqualsVisitor::operator()(const BencodeList& lhs,
                                                const BencodeList& rhs) const {
      return testEqualityOn_(lhs, rhs, [this](auto const& l, auto const& r) {
        return boost::apply_visitor(*this, l.get(), r.get());
      });
    }
    bool BencodeStrictEqualsVisitor::operator()(
        const BencodeDictionary& lhs, const BencodeDictionary& rhs) const {
      return testEqualityOn_(lhs, rhs, [this](auto const& l, auto const& r) {
        return boost::apply_visitor(*this, l.second.get(), r.second.get());
      });
    }

    // =========================================================================
    //        Class:  IndentOutputFilter
    // =========================================================================
    IndentOutputFilter::IndentOutputFilter(std::size_t indent, bool new_line)
        : indent_{indent}, new_line_{new_line} {}

    // =========================================================================
    //        Class:  BencodePrinter
    // =========================================================================
    const std::size_t BencodePrinter::INDENT_ = 2;
    BencodePrinter::BencodePrinter(std::ostream& os) : os_{os} {}

    void BencodePrinter::operator()(const BencodeInt& i) const {
      os_ << i << std::endl;
    }
    void BencodePrinter::operator()(const BencodeString& str) const {
      os_ << str << std::endl;
    }

    void BencodePrinter::operator()(const BencodeList& list) const {
      os_ << "[" << std::endl;
      auto ostream_ptr = makeFilteredOstream_(INDENT_, true);
      BencodePrinter visitor{*ostream_ptr};
      std::for_each(list.begin(), list.end(), [&](const auto& wrapper) {
        boost::apply_visitor(visitor, wrapper.get());
      });
      os_ << "]" << std::endl;
    }

    void BencodePrinter::operator()(const BencodeDictionary& dic) const {
      os_ << "{" << std::endl;
      std::for_each(dic.begin(), dic.end(),
                    [this](const auto& pair) { this->printPair_(pair); });
      os_ << "}" << std::endl;
    }

    void BencodePrinter::printPair_(const BencodePair& p) const {
      using ostream_it = std::ostream_iterator<char>;
      auto filtered_os = makeFilteredOstream_(getHashIndent_(p.first), false);
      std::fill_n(ostream_it{*filtered_os}, INDENT_, ' ');  // prepend spaces
      *filtered_os << p.first << " = " << std::flush;
      boost::apply_visitor(BencodePrinter{*filtered_os}, p.second.get());
    }

    BencodePrinter::OstreamPtr BencodePrinter::makeFilteredOstream_(
        size_t indent, bool new_line) const {
      auto filtered_os = std::make_unique<FilteringOstream>();
      filtered_os->push(IndentOutputFilter{indent, new_line});
      filtered_os->push(os_);
      return filtered_os;
    }

    std::size_t BencodePrinter::getHashIndent_(const BencodeString& key) const {
      return INDENT_ + key.size() + 3;
    }
  }  // -----  end of namespace detail  -----

  std::ostream& operator<<(std::ostream& os, const Bencode& bencode) {
    boost::apply_visitor(detail::BencodePrinter{os}, bencode);
    return os;
  }
  bool operator==(const Bencode& lhs, const Bencode& rhs) {
    return boost::apply_visitor(detail::BencodeStrictEqualsVisitor(), lhs, rhs);
  }
  bool operator!=(const Bencode& lhs, const Bencode& rhs) {
    return !(lhs == rhs);
  }
}  // -----  end of namespace bencode  -----
}  // -----  end of namespace torr  -----
